from random import randrange


class Car:
    fuel = 0
    trip_distance = 0
    model = ''
    color = ''

    def __init__(self, fuel_car, trip_distance_car, model_car, color_car):
        self.fuel = fuel_car
        self.trip_distance = trip_distance_car
        self.model = model_car
        self.color = color_car

    def move(self):
        """

        Returns (int):distance traveled and fuel remaining

        """
        distance = randrange(0, 9)
        self.trip_distance += distance
        self.fuel -= distance
        return self.trip_distance, self.fuel


mazda = Car(randrange(10, 20), 0, 6, 'black')
honda = Car(randrange(10, 20), 0, 'accord', 'white')
toyota = Car(randrange(10, 20), 0, 'camry', 'blue')

desired_dist = 5
race_dist = 0

while race_dist < desired_dist:
    mazda.move()
    race_dist = mazda.trip_distance
    if race_dist >= desired_dist:
        print('mazda win, distance: ', race_dist)
        break
    honda.move()
    race_dist = honda.trip_distance
    if race_dist >= desired_dist:
        print('honda win, distance: ', race_dist)
        break
    toyota.move()
    race_dist = toyota.trip_distance
    if race_dist >= desired_dist:
        print('toyota win, distance: ', race_dist)
        break

print('Race result:')
print(f'Mazda distance: {mazda.trip_distance}, remaining fuel: {mazda.fuel}')
print(f'Honda distance: {honda.trip_distance}, remaining fuel: {honda.fuel}')
print(f'Toyota distance: {toyota.trip_distance}, remaining fuel: {toyota.fuel}')
