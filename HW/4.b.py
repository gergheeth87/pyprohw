def union_dictionaries(first_dict, second_dict):
    """

    Args:
        first_dict (dict): get the first dict
        second_dict (dict): get the second dict

    Returns (dict): union of two dictionaries

    """
    new_dict = first_dict.copy()
    new_dict.update(second_dict)
    return new_dict


dict_one = {'car': 'Mazda', 'model': 6, 'fuel_type': 'gas'}
dict_two = {'color': 'white', 'max_speed': 230}

result = union_dictionaries(dict_one, dict_two)
print(result)
