def division_of_numbers(first_num, second_num):
    """

    Args:
        first_num (int): get the first number
        second_num (int): get the second number

    Returns (int): whole part and remainder from division

    """
    integer_division_result = first_num // second_num
    remainder_division_result = first_num % second_num
    return (f'The result of division numbers {first_num} and {second_num} is \
the whole part {integer_division_result} and the remainder {remainder_division_result} ')


test_num_one = 18
test_num_two = 7

result = division_of_numbers(test_num_one, test_num_two)
print(result)
