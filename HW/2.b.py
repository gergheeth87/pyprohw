def sum_of_numbers(first_num, second_num):
    """

    Args:
        first_num (int): get first number
        second_num (int): get second number

    Returns (int): get the sum of a numbers

    """
    addition_result = first_num + second_num
    return f'The sum of numbers {first_num} and {second_num} is {addition_result}'


test_num_one = 5
test_num_two = 7

result = sum_of_numbers(test_num_one, test_num_two)
print(result)
