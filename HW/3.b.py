def common_elements(first_list, second_list):
    """

    Args:
        first_list (list): get the first list
        second_list (list): get the second list

    Returns (list): common elements of both lists

    """
    new_list = list(set(first_list) & set(second_list))
    return f'Common elements of two lists : {new_list}'


test_list_one = ['A', 'B', 'C', 'D']
test_list_two = [4, 7, 'B', 'E', 'D']

result = common_elements(test_list_one, test_list_two)
print(result)
