from statistics import mean


def average_value(list_of_num):
    """

    Args:
        list_of_num (list): get a list of numbers

    Returns (float): the average of a list of numbers

    """
    average_result = mean(list_of_num)
    return f'The average of the list is {average_result}'


test_num_list = [1, 5, 7, 9]

result = average_value(test_num_list)
print(result)
