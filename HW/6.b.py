def list_of_pairs(test_list):
    """

    Args:
        test_list (list): get the list of numbers

    Returns (list): a list of even numbers

    """
    new_pair_list = []
    for i in test_list:
        if i % 2 == 0:
            new_pair_list.append(i)
    return new_pair_list


list_one = [1, 2, 3, 4, 5, 6, 7, 8]

result = list_of_pairs(list_one)
print(result)
