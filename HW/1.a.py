def str_len(arg):
    """

    Args:
        arg (str): get a string

    Returns (int): string length

    """
    string_length = len(arg)
    if string_length >= 1:
        return f'String length: {string_length} symbols'
    elif string_length == 0:
        print('Empty string, try again')


test_string = 'Test'
result = str_len(test_string)
print(result)
