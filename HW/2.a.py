def square_number(number):
    """

    Args:
        number (int): get a number

    Returns (int): square of a number

    """
    square_result = number ** 2
    return f'The square of a number {number} is {square_result}'


test_number = 5

result = square_number(test_number)
print(result)
