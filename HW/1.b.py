def concatenated_string(first_str, second_str):
    """

    Args:
        first_str (str): get the first string
        second_str (str): get the second string

    Returns (str): get concatenated string

    """
    first_str_len = len(first_str)
    second_str_len = len(second_str)
    if first_str_len >= 1 and second_str_len >= 1:
        concatenation_result = first_str + second_str
        return concatenation_result
    elif first_str_len == 0:
        print('Empty first string, try again')
    elif second_str_len == 0:
        print('Empty second string, try again')


str_one = 'One'
str_two = 'Two'

result = concatenated_string(str_one, str_two)
print(result)
