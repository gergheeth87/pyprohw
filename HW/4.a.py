def dict_keys(test_dict):
    """

    Args:
        test_dict (dict):  get a dictionary

    Returns (dict_keys): get a dict keys

    """
    return test_dict.keys()


dict_one = {'name': 'Vlad', 'age': 26}

result = dict_keys(dict_one)
print(result)
