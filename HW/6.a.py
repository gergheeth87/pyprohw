def parity_check(arg):
    """

    Args:
        arg (int): get the int number

    Returns (str):even if the number is even else odd

    """
    if arg == 0:
        return 'Zero'
    elif arg % 2 == 0:
        return f'{arg} is even number'
    else:
        return f'{arg} is odd number'


result = parity_check(7)
print(result)
