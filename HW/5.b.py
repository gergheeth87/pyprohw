def subset_validation(first_set, second_set):
    """

    Args:
        first_set (set): get the first set
        second_set (set): get the second set

    Returns (bool): True if a subset of the other else False

    """
    if first_set < second_set:
        return True
    else:
        return False


set_1 = {1, 2, 3, 4, 5}
set_2 = {1, 2, 3, 4, 5, 6}

result = subset_validation(set_1, set_2)
print(result)
