def union_of_sets(first_set, second_set):
    """

    Args:
        first_set (set): get the first set
        second_set (set): get the second set

    Returns (set): union of two sets

    """
    union_result = first_set | second_set
    return union_result


set_1 = {1, 2, 3, 4, 5}
set_2 = {1, 2, 3, 4, 5, 6}

result = union_of_sets(set_1, set_2)
print(result)
